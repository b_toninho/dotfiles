# Use NSS shared database for Thunderbird and Firefox
export NSS_DEFAULT_DB_TYPE="sql"
export JAVAFX_HOME="/usr/share/java/openjfx/jre/lib/ext/"
export LD_LIBRARY_PATH="/data/z3-4.5.0-x64-ubuntu-14.04/bin"
export CAML_LD_LIBRARY_PATH="$HOME/.opam/system/lib/stublibs:/usr/lib/ocaml/stublibs"
export MANPATH="$HOME/.opam/system/man:/usr/share/man:/usr/local/man"
export PERL5LIB="$HOME/.opam/system/lib/perl5"
export OCAML_TOPLEVEL_PATH="$HOME/system/lib/toplevel"



export PATH="$HOME/.opam/system/bin:$HOME/.cargo/bin:/data/Isabelle2016-1/bin:/data/clang/bin:/data/yices-1.0.40/bin:/data/z3-4.5.0-x64-ubuntu-14.04/bin:$HOME/Documents/Spin/Src6.4.6/:$HOME/Documents/Isabelle2016/bin/:$HOME/Documents/lem/bin:$HOME/.cabal/bin:/opt/cabal/1.20/bin:$HOME/.stack/programs/x86_64-linux/ghc-7.10.1/bin:/opt/ghc/7.8.4/bin:$PATH"

#export PATH="/data/clang/bin:/data/yices-1.0.40/bin:/data/z3-4.5.0-x64-ubuntu-14.04/bin:$HOME/Documents/Spin/Src6.4.6/:$HOME/Documents/Isabelle2016/bin/:$HOME/Documents/lem/bin:$HOME/.cabal/bin:/opt/cabal/1.20/bin:$HOME/.stack/programs/x86_64-linux/ghc-7.10.1/bin:/opt/ghc/7.8.4/bin:$PATH"
